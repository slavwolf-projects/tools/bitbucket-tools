#!/usr/bin/python3
import sys
import os
from getpass import getpass

import stashy

USER = "someuser"
PASS = getpass("Password for {}: ".format(USER))
STASH_URL = "https://bitbucket.com/"
PROJECT_PATH = "/home/user/Work/{}".format(sys.argv[1])

if not os.path.exists(sys.argv[1]):
    os.makedirs(PROJECT_PATH)
os.chdir(PROJECT_PATH)

stash = stashy.connect(STASH_URL, USER, PASS)

for repo in stash.projects[sys.argv[1]].repos.list():
    for url in repo["links"]["clone"]:
        if (url["name"] == "http"):
            os.system("git clone {}".format(url["href"]))
            break
